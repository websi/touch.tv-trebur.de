require_recipe "apt"
require_recipe "apache2"

execute "disable-default-site" do
  command "sudo a2dissite default"
  notifies :reload, resources(:service => "apache2"), :delayed
end

directory "/var/www/touch.tv-trebur.de" do
  owner "vagrant"
  group "www-data"
  mode "0755"
  action :create
end

web_app "project" do
  template "web_app.conf.erb"
  docroot "/var/www/touch.tv-trebur.de"
  server_name "touch.tv-trebur.dev"
  server_aliases []
  notifies :reload, resources(:service => "apache2"), :delayed
end
