/**
 * @class TvTrebur.controller.Handball
 * @author Timo Webler
 */
Ext.define('TvTrebur.controller.Main', {
	extend: 'Ext.app.Controller',

	requires: [
	],

	config: {
		routes: {
			'': 'onHomeButtonTap',
			'home': 'showHomeView',
			'site-notice': 'showSiteNoticeView',
			'news': 'showNewsView'
		},
		refs: {
			'mainPanel' : 'TvTrebur-view-Main',
			'siteNoticePanel' : 'TvTrebur-view-main-SiteNotic',
			'buttonGroup': 'TvTrebur-view-main-Toolbar segmentedbutton',
			'homeButton': 'TvTrebur-view-main-Toolbar segmentedbutton button[iconCls="home"]',
			'handballButton': 'TvTrebur-view-main-Toolbar segmentedbutton button[iconCls="team"]',
			'siteNoticeButton': 'TvTrebur-view-main-Toolbar segmentedbutton button[iconCls="info"]',
			'newsButton': 'TvTrebur-view-main-Toolbar segmentedbutton button[iconCls="favorites"]'
		},
		control: {
			homeButton: {
				tap: 'onHomeButtonTap'
			},
			handballButton: {
				tap: 'onHandballButtonTap'
			},
			siteNoticeButton: {
				tap: 'onSiteNoticeButtonTap'
			},
			newsButton: {
				tap: 'onNewsButtonTap'
			},
			goToDesktopButton: {
				tap: 'onGoToDesktopButtonTap'
			}
		},
		models: [
		],
		views: [
			'TvTrebur.view.main.Home',
			'TvTrebur.view.main.SiteNotice',
			'TvTrebur.view.handball.Navigation'
		]
	},

	/**
	 * Callback for home button tap
	 */
	onHomeButtonTap: function() {
		this.redirectTo('home');
	},

	/**
	 * Callback for handball button tap
	 */
	onHandballButtonTap: function() {
		this.redirectTo('handball');
	},

	/**
	 * Callback for site notice button tap
	 */
	onSiteNoticeButtonTap: function() {
		this.redirectTo('site-notice');
	},

	/**
	 * Callback for news button tap
	 */
	onNewsButtonTap: function() {
		this.redirectTo('news');
	},

	/**
	 * Show home screen and set home button to active
	 */
	showHomeView: function(){
		this.activateView('TvTrebur-view-main-Home', 'TvTrebur.view.main.Home', this.getHomeButton());
	},

	/**
	 * Show site notice screen and set home button to active
	 */
	showSiteNoticeView: function(){
		this.activateView('TvTrebur-view-main-SiteNotice', 'TvTrebur.view.main.SiteNotice', this.getSiteNoticeButton());
	},

	/**
	 * Show news screen and set home button to active
	 */
	showNewsView: function(){
		this.activateView('TvTrebur-view-news-Navigation', 'TvTrebur.view.news.Navigation', this.getNewsButton());
	},

	/**
	 * Activate view at mainpanel
	 *
	 * @param {String} itemId
	 * @param {Ext.Button} button
	 */
	activateView: function(itemId, className, button) {
			// activate home button
		this.getButtonGroup().setPressedButtons([button]);
			// show home view
		var view = this.getMainPanel().getComponent(itemId);
		if (!Ext.isObject(view)) {
			view = Ext.create(className);
			this.getMainPanel().add(view);
		}
		this.getMainPanel().setActiveItem(view);
	}
});