/**
 * @class TvTrebur.controller.Handball
 * @author Timo Webler
 */
Ext.define('TvTrebur.controller.Handball', {
	extend: 'Ext.app.Controller',

	requires: [
		'Ext.util.DelayedTask',
		'TvTrebur.view.handball.team.Table',
		'TvTrebur.view.handball.team.NextGames',
		'TvTrebur.view.handball.team.LastGames',
		'TvTrebur.view.handball.team.SelectView',
		'TvTrebur.store.handball.Team'
	],

	config: {
		routes: {
			'handball': 'showList',
			'handball/:urlName/table': 'showTableByUrlName',
			'handball/:urlName/lastGames': 'showLastGamesByUrlName',
			'handball/:urlName/nextGames': 'showNextGamesByUrlName'
		},
		refs: {
			'mainPanel' : 'TvTrebur-view-Main',
			'listView': 'TvTrebur-view-handball-List',
			'navigation': 'TvTrebur-view-handball-Navigation',
			'toolbar' : 'TvTrebur-view-handball-Navigation titlebar',
			'buttonGroup': 'TvTrebur-view-main-Toolbar segmentedbutton',
			'handballButton': 'TvTrebur-view-main-Toolbar segmentedbutton button[iconCls="team"]',
			'handballSelectViewButton': 'TvTrebur-view-handball-Navigation titlebar button',
			'handballSelectView': 'TvTrebur-view-handball-team-SelectView list'
		},
		control: {
			listView: {
				itemtap: 'onItemTapAction'
			},
			handballSelectViewButton: {
				tap: 'onHandballSelectViewButtonTap'
			},
			handballSelectView: {
				itemtap: 'onHandballSelectViewTap'
			}
		},
		models: [
			'TvTrebur.model.handball.Team'
		],
		stores: [
		],
		views: [
			'TvTrebur.view.handball.List',
			'TvTrebur.view.handball.team.Table',
			'TvTrebur.view.handball.team.NextGames',
			'TvTrebur.view.handball.team.LastGames'
		],
		currentTeamView: 'table'
	},

	/**
	 * on list item tap decide ifshow edit view or check item
	 *
	 * @param {Ext.dataview.DataView} dataView
	 * @param {Number} index
	 * @param {Ext.Element/Ext.dataview.component.DataItem} dataItem
	 * @param {Ext.data.Model} record
	 * @param {Ext.EventObject} eventObject
	 * @param {Object} eOpts
	 */
	onItemTapAction: function(dataView, index, dataItem, record, eventObject, eOpts) {
		var urlName = record.get('urlName');
		this.redirectTo('handball/' + urlName + '/table');
	},

	/**
	 *
	 * @param {Ext.Button} button
	 * @param {} Event
	 * @param {Object} eventOptions
	 */
	onHandballSelectViewButtonTap: function(button, event, eventOptions) {
		var view = Ext.Viewport.getComponent('TvTrebur-view-handball-team-SelectView');
		if (!Ext.isObject(view)) {
			view = Ext.create('TvTrebur.view.handball.team.SelectView');
		}
		if (!view.getParent()) {
			Ext.Viewport.add(view);
		}

		var viewList = this.getHandballSelectView();
		var record = viewList.getStore().findRecord('value', this.getCurrentTeamView());
		viewList.select(record, false, true);
		view.showBy(this.getHandballSelectViewButton());
	},

	/**
	 * on select view item tap
	 *
	 * @param {Ext.dataview.DataView} dataView
	 * @param {Number} index
	 * @param {Ext.Element/Ext.dataview.component.DataItem} dataItem
	 * @param {Ext.data.Model} record
	 * @param {Ext.EventObject} eventObject
	 * @param {Object} eOpts
	 */
	onHandballSelectViewTap: function(dataView, index, dataItem, record, eventObject, eOpts) {
		dataView.getParent().hide();
		var urlName = this.getNavigation().getActiveItem().getRecord().get('urlName');
		var value = record.get('value');
		this.redirectTo('handball/' + urlName + '/' + value);
	},

	/**
	 * Show list view
	 */
	showList: function() {
		this.loadStore(Ext.bind(this.activateHandballList, this));
	},

	/**
	 * Activate handball view
	 *
	 * @param {Number} name
	 */
	showLastGamesByUrlName: function(urlName) {
		this.loadStore(Ext.bind(this.showLastGamesView, this, [urlName], false));
	},

	/**
	 * Activate handball view
	 *
	 * @param {Number} name
	 */
	showTableByUrlName: function(urlName) {
		this.loadStore(Ext.bind(this.showTableView, this, [urlName], false));
	},

	/**
	 * Activate handball view
	 *
	 * @param {Number} name
	 */
	showNextGamesByUrlName: function(urlName) {
		this.loadStore(Ext.bind(this.showNextGamesView, this, [urlName], false));
	},

	/**
	 * Load store
	 *
	 * @param {Function} callback
	 */
	loadStore: function(callback) {
		/** @type {Ext.data.Store} store */
		var store = Ext.data.StoreManager.lookup('handballTeamStore');
			// create store if not exists
		if (!Ext.isObject(store)) {
			store = Ext.create('TvTrebur.store.handball.Team');
		}
			// load store if not loaded
		if (!store.isLoaded()) {
			store.load({
				callback: callback,
				scope: this
			});
		} else {
			callback.call();
		}
	},

	/**
	 * Activate handball navigation view
	 */
	activateHandballList: function() {
			// activate home button
		this.getButtonGroup().setPressedButtons([this.getHandballButton()]);
		if (Ext.isObject(this.getHandballSelectViewButton())) {
			this.getHandballSelectViewButton().hide();
		}
			// show home view
		var handballView = this.getMainPanel().getComponent('TvTrebur-view-handball-Navigation');
		if (!Ext.isObject(handballView)) {
			handballView = Ext.create('TvTrebur.view.handball.Navigation');
			this.getMainPanel().add(handballView);
		}
		handballView.setActiveItem(0);
		this.getMainPanel().setActiveItem(handballView);
		this.getToolbar().setTitle('Handball');
	},

	/**
	 * Show handball last games view
	 *
	 * @param {String} urlName
	 */
	showLastGamesView: function(urlName) {
		this.setCurrentTeamView('lastGames');
		this.activateView('TvTrebur-view-handball-team-LastGames', 'TvTrebur.view.handball.team.LastGames', urlName);
	},

	/**
	 * Show handball table view
	 *
	 * @param {String} urlName
	 */
	showTableView: function(urlName) {
		this.setCurrentTeamView('table');
		this.activateView('TvTrebur-view-handball-team-Table', 'TvTrebur.view.handball.team.Table', urlName);
	},

	/**
	 * Show handball table view
	 *
	 * @param {String} urlName
	 */
	showNextGamesView: function(urlName) {
		this.setCurrentTeamView('nextGames');
		this.activateView('TvTrebur-view-handball-team-NextGames', 'TvTrebur.view.handball.team.NextGames', urlName);
	},

	/**
	 * Activate view at handball view
	 *
	 * @param {String} itemId
	 * @param {Ext.Button} button
	 */
	activateView: function(itemId, className, urlName) {
		this.activateHandballList();
		this.getHandballSelectViewButton().show();

		/** @type {Ext.data.Store} store */
		var store = Ext.data.StoreManager.lookup('handballTeamStore');
		var handball = store.findRecord('urlName', urlName);

		var view = this.getNavigation().getComponent(itemId);
		if (!Ext.isObject(view)) {
			view = Ext.create(className);
			this.getNavigation().add(view);
		}
		view.setRecord(handball);
		this.getToolbar().setTitle(handball.get('name'));
		this.getNavigation().setActiveItem(view);
	}

});