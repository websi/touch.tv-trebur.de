/**
 * @class TvTrebur.controller.News
 * @author Timo Webler
 */
Ext.define('TvTrebur.controller.News', {
	extend: 'Ext.app.Controller',

	requires: [
		'TvTrebur.view.news.Navigation'
	],
	config: {
		models: [
			'TvTrebur.model.RssFeed'
		],
		stores: [
		],
		views: [
			'TvTrebur.view.news.List'
		],
		refs: {
			'navigationView': 'TvTrebur-view-news-Navigation',
			'listView': 'TvTrebur-view-news-List'
		},
		control: {
			listView: {
				itemtap: 'onNewsItemTapAction'
			}
		}
	},


	/**
	 * on list news item tap show news detail pages
	 *
	 * @param {Ext.dataview.DataView} dataView
	 * @param {Number} index
	 * @param {Ext.Element/Ext.dataview.component.DataItem} dataItem
	 * @param {Ext.data.Model} record
	 * @param {Ext.EventObject} eventObject
	 * @param {Object} eOpts
	 */
	onNewsItemTapAction: function(dataView, index, dataItem, record, eventObject, eOpts) {

		this.getNavigationView().push({
			xtype: 'TvTrebur-view-news-Detail',
			record: record
		});
	}
});