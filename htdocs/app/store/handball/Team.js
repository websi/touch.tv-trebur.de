/**
 * @class TvTrebur.store.handball.Team
 * @author Timo Webler
 */
Ext.define('TvTrebur.store.handball.Team', {
	extend: 'Ext.data.Store',

	requires: [
		'Ext.ux.proxy.AjaxCache'
	],

	config: {
		model: 'TvTrebur.model.handball.Team',
		autoLoad: false,
		storeId: 'handballTeamStore',
		proxy: {
			type: 'ajaxcache',
			cacheKey: 'handball_teams',
				// Cache 1 week
			cacheTimeout: 604800,
			noCache: false,
			url : 'data/handball_teams.json',
			reader: {
				type: 'json',
				rootProperty: 'teams'
			}
		}
	}
});