/**
 * @class TvTrebur.store.NewsFeed
 * @author Timo Webler
 */
Ext.define('TvTrebur.store.NewsFeed', {
	extend: 'Ext.data.Store',

	requires: [
		'Ext.ux.proxy.JsonPCache'
	],

	config: {
		model: 'TvTrebur.model.RssFeed',
		autoLoad: true,
		storeId: 'newsFeed',
		proxy: {
			type: 'jsonpcache',
			cacheKey: 'newsfeeds',
			cacheTimeout: 3600,
			noCache: false,
			url : 'http://query.yahooapis.com/v1/public/yql?' +
				Ext.Object.toQueryString({
					q: 'select * from feed where url=\'http://www.tv1886-trebur.de/index.php?id=280&type=100\'',
					format: 'json'
				}),

			reader: {
				type: 'json',
				rootProperty: 'query.results.item',
				totalProperty: 'query.count'
			},
			sorters: [
				{
					property: 'pubDate',
					direction: 'DESC'
				}
			]
		}
	}
});