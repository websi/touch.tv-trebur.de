/**
 * @class TvTrebur.view.handball.List
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.List', {
	extend: 'Ext.dataview.List',
	alias: 'widget.TvTrebur-view-handball-List',

	requires: [
		'TvTrebur.plugin.PullRefresh'
	],

	config: {
		emptyText: 'Es wurden keine Teams gefunden.',
		store: 'handballTeamStore',
		itemTpl: '{name}',
		fullscreen: true,
		plugins: [
			{ xclass: 'TvTrebur.plugin.PullRefresh' }
		]
	}
});
