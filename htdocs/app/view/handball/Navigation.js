/**
 * @class TvTrebur.view.handball.Navigation
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.Navigation', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-handball-Navigation',

	requires: [
		'Ext.TitleBar',
		'Ext.field.Select',
		'TvTrebur.view.handball.List'
	],
	config: {
		itemId: 'TvTrebur-view-handball-Navigation',
		layout: 'card',
		items: [
			{
				xtype: 'titlebar',
				docked: 'top',
				title: 'Handball',
				items: [
					{
						align: 'left',
						iconCls: 'more',
						iconMask: true,
						hidden: true
					}
				]
			},
			{
				xtype: 'TvTrebur-view-handball-List'
			}
		]
	}
});
