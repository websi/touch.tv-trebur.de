/**
 * @class TvTrebur.view.handball.team.SelectView
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.team.SelectView', {
	extend: 'Ext.Panel',
	alias: 'widget.TvTrebur-view-handball-team-SelectView',

	config: {
		itemId: 'TvTrebur-view-handball-team-SelectView',
		modal: true,
		centered: true,
		cls: Ext.baseCSSPrefix + 'select-overlay',
		layout: 'fit',
		hideOnMaskTap: true,
		items: [
			{
				xtype: 'list',
				itemTpl: '<span class="x-list-label">{text}</span>',
				data: [
					{text: 'letzte Spiele', value: 'lastGames'},
					{text: 'Tabelle', value: 'table'},
					{text: 'nächste Spiele', value: 'nextGames'}
				]
			}
		]
	}
});
