/**
 * @class TvTrebur.view.handball.Table
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.team.Table', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-handball-team-Table',

	requires: [
		'TvTrebur.util.SisHelper',
		'TvTrebur.view.handball.team.SisContent',
		'Ext.plugin.PullRefresh'
	],
	config: {
		title: '',
		itemId: 'TvTrebur-view-handball-team-Table',
		scrollable: true,
		plugins: [
			{
				xclass: 'Ext.plugin.PullRefresh',
				refreshFn: function (pullRefreshInstanz) {
						// call refresh method from current class
					pullRefreshInstanz.getList().refreshContent();
				}
			}
		],
		items: [
			{xtype: 'TvTrebur-view-handball-team-SisContent'}
		],
		sisTableUrlTempalte: 'http://www.gatecom.de/jsexport/jscore.aspx?art=tabelle&liga={0}&style=costum&team={1}',
		listeners: {
			activate: 'addSisContent'
		},
		lastRecordId: 0
	},

	refreshContent: function() {
		this.setLastRecordId(0);
		this.addSisContent();
	},

	addSisContent: function() {
			// do not load twice
		var recordId = this.getRecord().get('id');
		if (this.getLastRecordId() != recordId) {
			var contentContainer = this.getComponent('TvTrebur-view-handball-team-SisContent');
			// set record id
			this.setLastRecordId(recordId);
				// clear html
			contentContainer.removeAll(true);
			var url =  Ext.util.Format.format(
				this.getSisTableUrlTempalte(),
				this.getRecord().get('sis'),
				this.getRecord().get('sisName')
			);

			var sisHelper = Ext.create('TvTrebur.util.SisHelper');
			sisHelper.addContent(url, contentContainer, 'Tabelle wird geladen');
		}
	},

	/**
	 * getStore method for 'Ext.plugin.PullRefresh'
	 *
	 * @return null
	 */
	getStore: function() {
		return null;
	}
});
