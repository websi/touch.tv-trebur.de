/**
 * @class TvTrebur.view.handball.Table
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.team.LastGames', {
	extend: 'TvTrebur.view.handball.team.Table',
	alias: 'widget.TvTrebur-view-handball-team-LastGames',

	requires: [
		'TvTrebur.view.handball.team.Table'
	],
	config: {
		title: '',
		itemId: 'TvTrebur-view-handball-team-LastGames',
		sisTableUrlTempalte: 'http://www.gatecom.de/jsexport/jscore.aspx?art=last15&liga={0}&style=costum&team={1}'
	}
});
