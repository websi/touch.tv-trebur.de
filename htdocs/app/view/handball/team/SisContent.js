/**
 * @class TvTrebur.view.handball.Table
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.team.SisContent', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-handball-team-SisContent',

	requires: [
		'Ext.Container'
	],

	config: {
		itemId: 'TvTrebur-view-handball-team-SisContent',
		styleHtmlContent: true,
		padding: 0
	}
});
