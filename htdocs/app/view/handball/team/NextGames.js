/**
 * @class TvTrebur.view.handball.Table
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.handball.team.NextGames', {
	extend: 'TvTrebur.view.handball.team.Table',
	alias: 'widget.TvTrebur-view-handball-team-NextGames',

	requires: [
		'TvTrebur.view.handball.team.Table'
	],
	config: {
		title: '',
		itemId: 'TvTrebur-view-handball-team-NextGames',
		sisTableUrlTempalte: 'http://www.gatecom.de/jsexport/jscore.aspx?art=next15&liga={0}&style=costum&team={1}'
	}
});
