/**
 * @class TvTrebur.view.Main
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.Main', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-Main',
	requires: [
		'TvTrebur.view.main.Toolbar'
	],
	config: {
		layout: 'card',
		items: [
			{xtype: 'TvTrebur-view-main-Toolbar'}
		]
	}
});
