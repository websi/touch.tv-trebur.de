/**
 * @class TvTrebur.view.news.List
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.news.List', {
	extend: 'Ext.dataview.List',
	alias: 'widget.TvTrebur-view-news-List',

	requires: [
		'TvTrebur.store.NewsFeed',
		'TvTrebur.view.news.Detail',
		'TvTrebur.plugin.PullRefresh'
	],

	config: {
		title: 'Nachrichten',
		emptyText: 'Es wurden keine Nachrichten gefunden.',
		store: {xclass: 'TvTrebur.store.NewsFeed'},
		itemTpl: '{title}',
		plugins: [
			{ xclass: 'TvTrebur.plugin.PullRefresh' }
		]
	}
});
