/**
 * @class TvTrebur.view.handball.Navigation
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.news.Navigation', {
	extend: 'Ext.NavigationView',
	alias: 'widget.TvTrebur-view-news-Navigation',

	requires: [
		'Ext.NavigationView',
		'TvTrebur.view.news.List'
	],
	config: {
		itemId: 'TvTrebur-view-news-Navigation',
		title: 'Nachrichten',
		fullscreen: true,
		items: [
			{
				xtype: 'TvTrebur-view-news-List'
			}
		]
	}
});
