/**
 * @class TvTrebur.view.news.Detail
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.news.Detail', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-news-Detail',

	requires: [

	],
	config: {
		title: 'Nachrichten',
		scrollable: true,
		styleHtmlContent: true,
		listeners: {
			activate: 'addNewsContent'
		}
	},

	addNewsContent: function() {
		/** @type Ext.data.Model} record */
		var record = this.getRecord();
		var template = new Ext.XTemplate(
			'<div class="x-title title-wrap">{title}<br />{pubDate:date("d.m.Y")}</div>',
			'{encoded}',
			'<tpl if="enclosure">',
				'<br /><br /><a href="{enclosure.url}">{enclosure.url}</a>',
			'</tpl>'
		);
		template.compile();
		this.setHtml(template.apply(record.getData()));
	}
});
