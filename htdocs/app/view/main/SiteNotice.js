/**
 * @class TvTrebur.view.SiteNotice
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.main.SiteNotice', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-main-SiteNotice',

	requires: [
		'Ext.Panel'
	],
	config: {
		scrollable: true,
		itemId: 'TvTrebur-view-main-SiteNotice',
		styleHtmlContent: true,
		html: '<p>' +
					'<b>Öffnungszeiten: </b><br />' +
					'Montag 17.30 Uhr bis 19.30 Uhr<br />' +
					'Mittwoch 09.15 Uhr bis 12.15 Uhr<br />' +
					'Jeden 1. und 3. Samstag im Monat 10.00 Uhr bis 12.00 Uhr' +
				'</p>' +
				'<p>' +
					'<b>Adresse:</b> Astheimer Straße (Tennisanlage), 65468 Trebur<br />' +
					'<b>Postfachadresse: </b>Turnverein 1886 e.V. Trebur, Postfach 4, 65463 Trebur<br />' +
					'<b>Telefon:</b> 06147 / 50198 -22 Geschäftsstelle, 06147 / 1454 Buchungen Tennishalle<br />' +
					'<b>Email:</b> geschaeftsstelle@tv1886-trebur.de<br />' +
					'<b>Internet:</b> <a href="http://www.tv1886-trebur.de">www.tv1886-trebur.de</a>' +
				'</p>',
		items : [
			{
				xtype: 'titlebar',
				docked: 'top',
				title: 'Geschäftsstelle'
			}
		]
	}
});
