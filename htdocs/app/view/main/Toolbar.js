/**
 * @class TvTrebur.view.Main
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.main.Toolbar', {
	extend: 'Ext.Toolbar',
	alias: 'widget.TvTrebur-view-main-Toolbar',
	requires: [
		'Ext.SegmentedButton',
		'Ext.Button'
	],
	config: {
		docked: 'bottom',
		height: 55,
		items: [
			{xtype: 'spacer'},
			{
				xtype: 'segmentedbutton',
				allowDepress: false,
				allowMultiple: false,
				defaults: {
					xtype: 'button',
					height: 45,
					iconMask: true,
					iconAlign: 'top',
					align: 'center',
					ui: 'plain'
				},
				items: [
					{
						text: 'Home',
						iconCls: 'home'
					},
					{
						text: 'Handball',
						iconCls: 'team'
					},
					{
						text: 'News',
						iconCls: 'favorites'
					},
					{
						text: 'Impressum',
						iconCls: 'info'
					}
				]
			},
			{xtype: 'spacer'}
		]
	}
});
