/**
 * @class TvTrebur.view.Home
 * @author Timo Webler
 */
Ext.define('TvTrebur.view.main.Home', {
	extend: 'Ext.Container',
	alias: 'widget.TvTrebur-view-main-Home',

	requires: [
		'Ext.Img'
	],
	config: {
		title: 'Home',
		layout: 'vbox',
		scrollable: true,
		styleHtmlContent: true,
		itemId: 'TvTrebur-view-main-Home',

		items: [
			{
				xtype: 'container',
				flex: 1
			}, {
				xtype: 'image',
				src: 'resources/images/logo_tv_trebur.png',
				height: 105,
				width: 105,
				cls: 'welcome-image'
			}, {
				xtype: 'container',
				styleHtmlContent: true,
				html: '<h1>Willkommen beim TV Trebur</h1>',
				cls: 'welcome-text',
				flex: 1
			}, {
				xtype: 'container',
				flex: 1
			}
		]
	}
});
