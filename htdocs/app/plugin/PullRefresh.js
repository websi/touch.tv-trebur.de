/**
 * @class TvTrebur.plugin.PullRefresh
 * @author Timo Webler
 */
Ext.define('TvTrebur.plugin.PullRefresh', {
	extend: 'Ext.plugin.PullRefresh',
	alias: 'plugin.TvTrebur-plugin-PullRefresh',

	/**
	 * @private
	 * Attempts to load the newest posts via the attached List's Store's Proxy.
	 * Clear local storage cache before laod data
	 */
	fetchLatest: function() {
		var store = this.getList().getStore(),
			proxy = store.getProxy();

		var key = proxy.getCacheKey();
		window.localStorage.setItem(key, Ext.JSON.encode({}));

		this.callParent();
	}
});