/**
 * @class TvTrebur.util.SisHelper
 * @author Timo Webler
 */
Ext.define('TvTrebur.util.SisHelper', {

	statics: {

		/**
		 *
		 * @type Ext.Component
		 */
		component: null,

		/**
		 * script call counter
		 * @type Number
		 */
		counter: 0,

		/**
		 * document.write backup
		 * @type Function
		 */
		backupDocumentWrite: null,

		/**
		 * call after script is loaded. Reset document write
		 */
		scriptOnLoadFunction: function() {
			TvTrebur.util.SisHelper.counter--;
			if (TvTrebur.util.SisHelper.counter == 0) {
				// reset document.write method
				document.write = TvTrebur.util.SisHelper.backupDocumentWrite;
				Ext.Viewport.unmask();
			}
		},

		/**
		 * call after script is loaded. Reset document write
		 */
		scriptOnErrorFunction: function() {
			TvTrebur.util.SisHelper.counter--;
			TvTrebur.util.SisHelper.documentWrite('Beim laden der Daten ist ein Fehler aufgetreten.');
			if (TvTrebur.util.SisHelper.counter == 0) {
				// reset document.write method
				document.write = TvTrebur.util.SisHelper.backupDocumentWrite;
				Ext.Viewport.unmask();
			}
		},

		/**
		 * Document write fake function
		 *
		 * @param {String} content
		 */
		documentWrite: function(content) {
				// do nothing for empty string
			if (content.length == 0) {
				return;
			}
				// do nothing for stylesheets
			if (content.substring(0,6) == '<style') {
				return;
			}

			content = TvTrebur.util.SisHelper.checkContentForScriptTags(content)

			TvTrebur.util.SisHelper.component.add([{
				xtype: 'container',
				html: content
			}]);
		},

		/**
		 * Check content for script tag and create call of this.addContent call.
		 * Remove script tags from content
		 * @param {String} content
		 * @return {String}
		 */
		checkContentForScriptTags: function(content) {
			var div = document.createElement('div');
			div.innerHTML = content;
			var childesToRemove = [];
				// search script tags in child nodes
			for(var i = 0; i < div.childNodes.length; i++) {
				var node = div.childNodes[i];
				var nodeName = node.nodeName;
				if (nodeName.toLocaleLowerCase() == 'script') {
						// script tag found
					var url = node.getAttribute('src');
					if (!Ext.isEmpty(url)) {
						childesToRemove.push(node);
						var sisHelper = new this()
						sisHelper.addContent(url, TvTrebur.util.SisHelper.component, '');
					}
				}
			}
				// remove script tags
			for (var i = 0; i < childesToRemove.length; i++) {
				div.removeChild(childesToRemove[i]);
			}

			return div.innerHTML;
		}
	},

	/**
	 * Add content of script url to component
	 *
	 * @param {String} url
	 * @param {Ext.Component} component
	 */
	addContent: function(url, component, loadText) {
		if (TvTrebur.util.SisHelper.counter == 0) {
			Ext.Viewport.mask({
				xtype: 'loadmask',
				message: loadText,
				indicator: true
			});
				// backup document.write method
			TvTrebur.util.SisHelper.backupDocumentWrite = document.write;
			TvTrebur.util.SisHelper.component =component;
				// override document.write method
			document.write = TvTrebur.util.SisHelper.documentWrite
		}

			// increase counter
		TvTrebur.util.SisHelper.counter++;

		/** @type {Ext.dom.Element} element */
		var element = Ext.get(TvTrebur.util.SisHelper.component.getId());
		var script = this.createScriptElementFromUrl(url);
		element.appendChild(script);
	},

	/**
	 * Create script element with src = url
	 * @param {String} url
	 * @return {DomElement}
	 */
	createScriptElementFromUrl: function(url) {
		var script = document.createElement('script');
		script.type = 'text/javascript';
		script.src = url;
		script.onload = TvTrebur.util.SisHelper.scriptOnLoadFunction;
		script.onerror = TvTrebur.util.SisHelper.scriptOnErrorFunction;
		return script;
	}

});