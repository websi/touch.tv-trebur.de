/**
 * @class TvTrebur.model.RssFeed
 * @author Timo Webler
 */
Ext.define('TvTrebur.model.RssFeed', {
	extend: 'Ext.data.Model',

	config: {
		idProperty: 'link',
		fields: [
			{name: 'title', type: 'string'},
			{name: 'link', type: 'string'},
			{name: 'description', type: 'string'},
			{name: 'encoded', type: 'string'},
			{name: 'category', type: 'string'},
			{name: 'pubDate', type: 'date'},
			{name: 'enclosure', type: 'auto'}
		]
	}
});