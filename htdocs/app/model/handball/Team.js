/**
 * @class TvTrebur.model.handball.Team
 * @author Timo Webler
 */
Ext.define('TvTrebur.model.handball.Team', {
	extend: 'Ext.data.Model',

	config: {
		fields: [
			{name: 'id', type: 'integer'},
			{name: 'name', type: 'string'},
			{name: 'urlName', type: 'string'},
			{name: 'sis', type: 'string'},
			{name: 'sisName', type: 'string'}
		]
	}
});