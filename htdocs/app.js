Ext.Loader.setPath({
	'Ext': 'sdk/src',
	'Ext.ux': 'ux'
});

Ext.application({
	name: 'TvTrebur',

	controllers: ["Main", "Handball", "News"],

	requires: [
		'Ext.MessageBox'
	],

	views: ['Main'],

	icon: {
		'57': 'resources/icons/icon_057.png',
		'72': 'resources/icons/icon_072.png'
	},
	glossOnIcon: false,
	isIconPrecomposed: false,

	launch: function() {
		// Destroy the #appLoadingIndicator element
		Ext.fly('appLoadingIndicator').destroy();

		// Initialize the main view
		Ext.Viewport.add(Ext.create('TvTrebur.view.Main'));
	},

	onUpdated: function() {
		Ext.Msg.confirm(
			"Application Update",
			"This application has just successfully been updated to the latest version. Reload now?",
			function(buttonId) {
				if (buttonId === 'yes') {
						// clear local storage on update
					window.localStorage.clear();
					window.location.reload();
				}
			}
		);
	}
});
